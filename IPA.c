#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <pwd.h>
#include <pthread.h>
#include <semaphore.h>
#include <poll.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#ifndef UNIX_PATH_MAX
#define UNIX_PATH_MAX 108 /* This should be in the system headers */
#endif
#include "IPApacket.h"

static char *queueDirName;         /* A private directory for root to manage the queue */
static char *IPA_QUEUE_DIR;        /* Environment variable containing host:queueDirName */
static FILE *daemonLogFile = NULL; /* log file in the queue dir */
static char *WINHOST = "Windows";
static char *LINHOST = "Linux";

typedef struct Slot          /* Data on Slots that are available */
{
    pid_t      pid;          /* If 0, slot free.  If 1, slot used on windows.  Else the pid of the ssh process */
    uid_t      uid;          /* The uid of the current user of this slot */
    char      *username;     /* The username of the user using this slot */
    char      *filename;     /* The queue filename running on this slot */
    int        clusterNum;   /* The cluster number running on this slot */
    int        jobNum;       /* The job number running on this slot */
    int        restartCount; /* Number of times to restart cluster if this job fails */
    char      *logname;      /* The logfile for this cluster */
} Slot;
typedef struct Host /* Information on the Hosts */
{
    char *type;     /* WINHOST or LINHOST */
    char *name;     /* The linux hostname, or for windows win-username. */
    char *owner;    /* username allowed to run jobs here */
    int   isUp;     /* Whether host is up or not */
    int   fd;       /* FD of tcp connection for windows */
    Slot *slots;
    int   numSlots;
} Host;
static pthread_mutex_t  hostsMutex = PTHREAD_MUTEX_INITIALIZER;
static Host            *hosts = NULL;
static int              numHosts = 0;

static pthread_mutex_t   lowPriorityQueueMutex = PTHREAD_MUTEX_INITIALIZER;
static char            **lowPriorityQueue = NULL; /* Queue is just an array of filenames */
static int               lowPriorityQueueSize = 0;

static pthread_mutex_t   highPriorityQueueMutex = PTHREAD_MUTEX_INITIALIZER;
static char            **highPriorityQueue = NULL; /* Queue is just an array of filenames */
static int               highPriorityQueueSize = 0;

static pthread_mutex_t priorityActiveMutex = PTHREAD_MUTEX_INITIALIZER;
static int             priorityActive = 0; /* If the high priority queue is working */

static pthread_mutex_t nextRunNumberMutex = PTHREAD_MUTEX_INITIALIZER;
static int             nextRunNumber = 1;  /* The next cluster number to put in the queue */

static pthread_mutex_t getpwMutex = PTHREAD_MUTEX_INITIALIZER;        /* Helgrind complains that getpwuid_r is not thread safe, so we will use getpwuid and mutex it */
static pthread_mutex_t jobsStartingMutex = PTHREAD_MUTEX_INITIALIZER; /* Mutex to keep WaitOnJobs from processing while jobs are still being started */

static uid_t activeUID; /* The UID that this program is running under */

static sem_t shutdownSemaphore;
static void SigInt(int sig){sem_post(&shutdownSemaphore);} /* A signal handler that is set up as a one shot handler */

static void *ExecuteClusters(void *arg); /* The threads that will watch the queue dir and start the jobs */
static void *WaitOnJobs(void *arg);      /* The thread that will catch when jobs are done */
static void *StatusServer(void *arg);    /* The thread that will serve the linux clients */
static void *WindowsServer(void *arg);   /* The thread that will serve the windows clients */
static void *WatchNodes(void *arg);      /* The thread that will update hosts based on changes in the nodes file */
static void *PingHosts(void *arg);       /* The thread that will update hosts based on changes in reachability */
static int ReadTorqueNodesFile(char *torqueNodesFilename); /* (Re)read the torque nodes file and update hosts */

static void Enqueue(int isHighPriority, char *filename)
{
    pthread_mutex_t   *queueMutex;
    char            ***queue;
    int               *queueSize;
    int q, numInQueue = 0;
    if (isHighPriority)
    {
        queueMutex = &highPriorityQueueMutex;
        queue      = &highPriorityQueue;
        queueSize  = &highPriorityQueueSize;
    }
    else
    {
        queueMutex = &lowPriorityQueueMutex;
        queue      = &lowPriorityQueue;
        queueSize  = &lowPriorityQueueSize;
    }
    pthread_mutex_lock(queueMutex);
    for (q=0; q<*queueSize; q++)
    {
        if ((*queue)[q])
            numInQueue++;
        else
            break;
    }
    if (numInQueue == *queueSize)
    {
        int oldQueueSize = *queueSize;
        if (*queueSize == 0)
            *queueSize = 32;
        else
            *queueSize *= 2;
        *queue = (char **)realloc(*queue, *queueSize * sizeof(char *));
        memset((*queue) + oldQueueSize, 0, ((*queueSize) - oldQueueSize) * sizeof(char *));
    }
    (*queue)[numInQueue] = filename;
    pthread_mutex_unlock(queueMutex);
}

static char *Dequeue(int isHighPriority)
{
    pthread_mutex_t   *queueMutex;
    char            ***queue;
    int               *queueSize;
    int q;
    char *returnValue = NULL;;
    if (isHighPriority)
    {
        queueMutex = &highPriorityQueueMutex;
        queue      = &highPriorityQueue;
        queueSize  = &highPriorityQueueSize;
    }
    else
    {
        queueMutex = &lowPriorityQueueMutex;
        queue      = &lowPriorityQueue;
        queueSize  = &lowPriorityQueueSize;
    }
    pthread_mutex_lock(queueMutex);
    if (*queue)
    {
        returnValue = (*queue)[0];
        for (q=0; q<(*queueSize) - 1; q++)
        {
            (*queue)[q] = (*queue)[q+1];
            if (!(*queue)[q])
                break;
        }
        (*queue)[(*queueSize) - 1] = NULL;
    }
    pthread_mutex_unlock(queueMutex);
    return returnValue;
}

static int SlotFillingAlgorithm(char *username, int numJobs, int *numCPUsInJob, int *slotsLeftOnHost, int *hostsToRunJobs, int *notEnoughWindowsSlots)
{   /* This must be called within a mutex lock for hostsMutex */
    /* hostsToRunJobs can be NULL to just test if there is ever enough resources to run the job */
    /* if it is not NULL, it means that we want to run the job now */
    /* notEnoughWindowsSlots is returned 1 if the failure is that we are waiting on windows slots to open */
    /* it can also be NULL if you aren't interested in that info */
    int h, j;
    if (notEnoughWindowsSlots) *notEnoughWindowsSlots=0;
    for (h=j=0; h<numHosts && j<numJobs;) /* First try to place windows jobs */
    {
        if (numCPUsInJob[j] < 0) /* This is a windows job */
        {
            if (hosts[h].type == WINHOST && (hosts[h].owner[0] == '*' || !strcmp(username, hosts[h].owner)) && slotsLeftOnHost[h] >= -numCPUsInJob[j])
            {
                slotsLeftOnHost[h] += numCPUsInJob[j];
                if (hostsToRunJobs) hostsToRunJobs[j] = h; /* If keeping track of hosts to run jobs */
                j++;   /* Go to next job, stay on this host */
            }
            else
            {
                h++;
            }
        }
        else
        {
            j++;
        }
    }
    if (j == numJobs) /* If all windows jobs placed */
    {
        for (h=j=0; h<numHosts && j<numJobs;) /* Now try to place linux jobs */
        {
            if (numCPUsInJob[j] > 0) /* This is a linux job */
            {
                if (hosts[h].type == LINHOST && slotsLeftOnHost[h] >= numCPUsInJob[j])
                {
                    slotsLeftOnHost[h] -= numCPUsInJob[j];
                    if (hostsToRunJobs) hostsToRunJobs[j] = h; /* If keeping track of hosts to run jobs */
                    j++;   /* Go to next job, stay on this host */
                }
                else
                {
                    h++;   /* Go to next host, stay on this job */
                }
            }
            else
            {
                j++;
            }
        }
    }
    else
    {
        if (notEnoughWindowsSlots) *notEnoughWindowsSlots=1;
    }
    return (j == numJobs);
}

static int ClusterCanRunEventually(char *username, int numJobs, int *numCPUsInJob, int *notEnoughWindowsSlots)
{
    int fillResult, *slotsLeftOnHost;
    pthread_mutex_lock(&hostsMutex);
    slotsLeftOnHost = (int *)calloc(numHosts, sizeof(int));
    if (slotsLeftOnHost)
    {
        int h;
        for (h=0; h<numHosts; h++)
            if (hosts[h].type && hosts[h].isUp)
                slotsLeftOnHost[h] = hosts[h].numSlots;
        fillResult = SlotFillingAlgorithm(username, numJobs, numCPUsInJob, slotsLeftOnHost, NULL, notEnoughWindowsSlots);
        free(slotsLeftOnHost);
    }
    else
    {
        fillResult = 0;
    }
    pthread_mutex_unlock(&hostsMutex);
    return fillResult;
}

static int *HostsToRunJobs(char *username, int numJobs, int *numCPUsInJob, int *notEnoughWindowsSlots)
{
    int fillResult, *slotsLeftOnHost, *hostsToRunJobs;
    pthread_mutex_lock(&hostsMutex);
    slotsLeftOnHost = (int *)calloc(numHosts, sizeof(int));
    if (slotsLeftOnHost)
    {
        hostsToRunJobs = (int *)calloc(numJobs, sizeof(int));
        if (hostsToRunJobs)
        {
            int h, s;
            for (h=0; h<numHosts; h++)
                if (hosts[h].type && hosts[h].isUp)
                    for (s=0; s<hosts[h].numSlots; s++)
                        if (hosts[h].slots[s].pid == 0)
                            slotsLeftOnHost[h]++;
            fillResult = SlotFillingAlgorithm(username, numJobs, numCPUsInJob, slotsLeftOnHost, hostsToRunJobs, notEnoughWindowsSlots);
            free(slotsLeftOnHost);
            if (!fillResult)
            {
                free(hostsToRunJobs);
                hostsToRunJobs = NULL;
            }
        }
        else
        {
            free(slotsLeftOnHost);
        }
    }
    else
    {
        hostsToRunJobs = NULL;
    }
    pthread_mutex_unlock(&hostsMutex);
    return hostsToRunJobs;
}

static int ClusterNumOfLowPriorityDirent(const struct dirent *d)
{
    return atoi(d->d_name);
}

static int ClusterNumOfHighPriorityDirent(const struct dirent *d)
{
    return d->d_name[0] == '_' ? atoi(d->d_name+1) : 0; /* High priority entries start with _ */
}

static int ClusterNumOfAnyPriorityDirent(const struct dirent *d)
{
    return d->d_name[0] == '_' ? atoi(d->d_name+1) : atoi(d->d_name);
}

static int QueueDirCompare(const struct dirent **a, const struct dirent **b)
{
    char absoluteFilenameL[FILENAME_MAX];
    char absoluteFilenameR[FILENAME_MAX];
    int l = (*a)->d_name[0] == '_' ? -atoi((*a)->d_name+1) : atoi((*a)->d_name); /* Make high priority clusters have a negative value */
    int r = (*b)->d_name[0] == '_' ? -atoi((*b)->d_name+1) : atoi((*b)->d_name); /* So they sort to be before low priority */
    struct stat statBufL;
    struct stat statBufR;
    snprintf(absoluteFilenameL, sizeof(absoluteFilenameL), "%s/%s", queueDirName, (*a)->d_name);
    snprintf(absoluteFilenameR, sizeof(absoluteFilenameR), "%s/%s", queueDirName, (*b)->d_name);
    if (stat(absoluteFilenameL, &statBufL) == 0 && stat(absoluteFilenameR, &statBufR) == 0)
    {
        int tl = statBufL.st_mtime;
        int tr = statBufR.st_mtime;
        if (l<0 && r<0) /* If they are both high priority */
        {
            if (tl < tr)      /* Compared based on file time */
            {
                return -1;
            }
            else if (tl > tr)
            {
                return 1;
            }
            else
            {
                if (l < r) return  1; /* Lower values are higher cluster numbers */
                if (l > r) return -1;
                return 0;
            }
        }
        else if (l>0 && r>0) /* If they are both low priority */
        {
            if (tl < tr)      /* Compared based on file time */
            {
                return -1;
            }
            else if (tl > tr)
            {
                return 1;
            }
        }
    } 
    if (l < r) return -1;
    if (l > r) return  1;
    return 0;
}

static char *writeTimestamp(char *ts, int len)
{
    time_t epochTime = time(NULL);
    struct tm localTime, *returnVal;
    returnVal = localtime_r(&epochTime, &localTime);
    if (returnVal)
        snprintf(ts, len, "%4d/%02d/%02d %02d:%02d:%02d",
                 localTime.tm_year + 1900,
                 localTime.tm_mon + 1,
                 localTime.tm_mday,
                 localTime.tm_hour,
                 localTime.tm_min,
                 localTime.tm_sec);
    else
        snprintf(ts, len, "%4d/%02d/%02d %02d:%02d:%02d", 0, 0, 0, 0, 0, 0);
    return ts;
}

static char *writeCondorTimestamp(char *ts, int len)
{
    time_t epochTime = time(NULL);
    struct tm localTime, *returnVal;
    returnVal = localtime_r(&epochTime, &localTime);
    if (returnVal)
        snprintf(ts, len, "%02d:%02d:%02d",
                 localTime.tm_hour,
                 localTime.tm_min,
                 localTime.tm_sec);
    else
        snprintf(ts, len, "%02d:%02d:%02d", 0, 0, 0);
    return ts;
}

static char *writeCondorDatestamp(char *ts, int len)
{
    time_t epochTime;
    struct tm localTime, *returnVal;
    epochTime = time(NULL);
    returnVal = localtime_r(&epochTime, &localTime);
    if (returnVal)
        snprintf(ts, len, "%02d/%02d",
                 localTime.tm_mon + 1,
                 localTime.tm_mday);
    else
        snprintf(ts, len, "%02d/%02d", 0, 0);
    return ts;
}

static void perror_to_log_and_flush(const char *attempted)
{
    char errBuf[32];
    char timestamp[32];
    char *r = strerror_r(errno, errBuf, sizeof(errBuf));
    if (attempted && *attempted)
        fprintf(daemonLogFile, "%s : %s : %s\n",
                writeTimestamp(timestamp, sizeof(timestamp)),
                attempted,
                r);
    else
        fprintf(daemonLogFile, "%s\n",
                r);
    fflush(daemonLogFile);
}

int main(int argc, char **argv)
{
    char timestamp[32];
    char torqueNodesFilename[FILENAME_MAX];
    pthread_t executePriorityClustersThread, executeClustersThread, waitOnJobsThread, statusServerThread, windowsServerThread, watchNodesThread, pingHostsThread;
    IPA_QUEUE_DIR = getenv("IPA_QUEUE_DIR");
    if (!IPA_QUEUE_DIR)
    {
        fprintf(stderr, "Please set $IPA_QUEUE_DIR that is used in this cloud.\n");
        return 1;
    }
    {   /* Check validity of IPA_QUEUE_DIR */
        char *cPos = strchr(IPA_QUEUE_DIR, ':');
        if (!cPos)
        {
            fprintf(stderr, "$IPA_QUEUE_DIR should be <this_hostname>:<queue_directory>\n");
            return 1;
        }
        queueDirName = cPos + 1;
    }
    IPA_QUEUE_DIR -= strlen("IPA_QUEUE_DIR=");
    {   /* Set up the signal handler */
        struct sigaction sa;
        sigset_t set;
        if (sigemptyset(&set) == -1)
        {
            perror("sigemptyset");
            return 1;
        }
        if (sem_init(&shutdownSemaphore, 0, 0) != 0)
        {
            perror("sem_init for signal handler");
            return 1;
        }
        sa.sa_handler = SigInt;
        sa.sa_mask = set;
        sa.sa_flags = SA_RESETHAND;
        if (sigaction(SIGINT, &sa, NULL) == -1)
        {
            perror("sigaction");
            return 1;
        }
        if (sigaction(SIGTERM, &sa, NULL) == -1)
        {
            perror("sigaction");
            return 1;
        }
        sa.sa_handler = SIG_IGN;
        sa.sa_flags = 0;
        if (sigaction(SIGPIPE, &sa, NULL) == -1)
        {
            perror("sigaction");
            return 1;
        }
    }
    {   /* This program should be run as root.  It can either be started by root or be chmod u+s */
        int origUID = getuid();
        int efUID = geteuid();
        if (origUID != 0) /* If not root */
        {
            if (efUID != 0) /* If not suid root */
            {
                fprintf(stderr, "This should be run as root or suid root.\n");
            }
            else if (setuid(0) == -1)
            {
                perror("setuid");
            }
        }
        activeUID = getuid();
    }
    {   /* Check queue dir */
        struct stat statBuf;
        snprintf(torqueNodesFilename, sizeof(torqueNodesFilename), "%s/nodes", queueDirName);
        if (stat(queueDirName, &statBuf) == 0)
        {
            if (!S_ISDIR(statBuf.st_mode))
            {
                fprintf(stderr, "Queue_Dir %s is not a directory\n", queueDirName);
                return 1;
            }
            if ((statBuf.st_mode & 0777) != 0711)
            {
                fprintf(stderr, "Queue_Dir %s should be mode 711 for security\n", queueDirName);
            }
        } 
        else if (mkdir(queueDirName, S_IRWXU | S_IXGRP | S_IXOTH) == -1)
        {
            fprintf(stderr, "Cannot make directory %s : ", queueDirName);
            perror("");
            return 1;
        }
    }
    {   /* Lock so only one process can handle this directory at a time */
        char daemonLogFilename[FILENAME_MAX];
        snprintf(daemonLogFilename, sizeof(daemonLogFilename), "%s/log", queueDirName);
        daemonLogFile = fopen(daemonLogFilename, "a");
        if (!daemonLogFile)
        {
            perror("Could not create log file");
            return 1;
        }
        if (flock(fileno(daemonLogFile), LOCK_EX | LOCK_NB) == -1)
        {
            perror("Could not lock the log file");
            return 1;
        }
    }
    {   /* Check to see what the next available cluster number is */
        DIR *queueDIR = opendir(queueDirName);
        if (!queueDIR)
        {
            fprintf(stderr, "Cannot open %s : ", queueDirName);
            perror("");
            return 1;
        }
        else
        {
            struct dirent *dirEntry;
            pthread_mutex_lock(&nextRunNumberMutex);
            while ((dirEntry = readdir(queueDIR)))
            {
                int v = atoi(dirEntry->d_name);
                if (v >= nextRunNumber)
                    nextRunNumber = v + 1;
            }
            pthread_mutex_unlock(&nextRunNumberMutex);
            closedir(queueDIR);
        }
    }
    if (ReadTorqueNodesFile(torqueNodesFilename)) return 1;
    if (daemon(0, 0) == -1)
    {
        perror("daemon");
        return 1;
    }
    {   /* Start other threads (execute, wait, status server) */
        if (pthread_create(&executeClustersThread, NULL, ExecuteClusters, NULL) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create thread to execute clusters.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            return 1;
        }
        if (pthread_create(&executePriorityClustersThread, NULL, ExecuteClusters, (void*)1) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create thread to execute clusters.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            return 1;
        }
        if (pthread_create(&waitOnJobsThread, NULL, WaitOnJobs, NULL) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create thread to wait on jobs.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            return 1;
        }
        if (pthread_create(&windowsServerThread, NULL, WindowsServer, NULL) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create thread to serve windows clients.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            return 1;
        }
        if (pthread_create(&statusServerThread, NULL, StatusServer, NULL) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create thread to serve linux clients.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            return 1;
        }
        if (pthread_create(&watchNodesThread, NULL, WatchNodes, torqueNodesFilename) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create thread to watch nodes file.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            return 1;
        }
        if (pthread_create(&pingHostsThread, NULL, PingHosts, NULL) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create thread to ping hosts.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            return 1;
        }
        fprintf(daemonLogFile, "%s : All threads started in process %d.\n", writeTimestamp(timestamp, sizeof(timestamp)), getpid());
        fflush(daemonLogFile);
    }
    while (sem_wait(&shutdownSemaphore) == -1 && errno == EINTR)
        continue;
    {   /* shutdown */
        int h, s;
        fprintf(daemonLogFile, "%s : Stopping IPA ...\n", writeTimestamp(timestamp, sizeof(timestamp)));
        fflush(daemonLogFile);
        pthread_cancel(statusServerThread);
        pthread_join(statusServerThread, NULL);
        pthread_cancel(windowsServerThread);
        pthread_join(windowsServerThread, NULL);
        pthread_cancel(watchNodesThread);
        pthread_join(watchNodesThread, NULL);
        pthread_cancel(pingHostsThread);
        pthread_join(pingHostsThread, NULL);
        pthread_cancel(executePriorityClustersThread);
        pthread_join(executePriorityClustersThread, NULL);
        pthread_cancel(executeClustersThread);
        pthread_join(executeClustersThread, NULL);
        pthread_cancel(waitOnJobsThread);
        pthread_join(waitOnJobsThread, NULL);
        pthread_mutex_lock(&hostsMutex);
        for (h=0; h<numHosts; h++)
        {
            if (hosts[h].type == WINHOST && hosts[h].fd != -1)
            {
                char buf[2048];
                IPApacket ipaPacket;
                ipaPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
                ipaPacket.packetType = IPApacketType_close;
                ipaPacket.flags = 0;
                ipaPacket.subflags = 0;
                memset(ipaPacket.buf, 0, sizeof(ipaPacket.buf));
                fprintf(daemonLogFile, "%s : Shutdown connection to %s.\n", writeTimestamp(timestamp, sizeof(timestamp)), hosts[h].name);
                write(hosts[h].fd, &ipaPacket, sizeof(ipaPacket));
                fprintf(daemonLogFile, "%s : Flush old data from %s.\n", writeTimestamp(timestamp, sizeof(timestamp)), hosts[h].name);
                while (read(hosts[h].fd, buf, sizeof(buf)) > 0);
                fprintf(daemonLogFile, "%s : Close connection to %s.\n", writeTimestamp(timestamp, sizeof(timestamp)), hosts[h].name);
                close(hosts[h].fd);
                hosts[h].isUp = 0;
                hosts[h].fd = -1;
            }
            if (hosts[h].name) free(hosts[h].name);
            hosts[h].name = NULL;
            if (hosts[h].owner) free(hosts[h].owner);
            hosts[h].owner = NULL;
            for (s=0; s<hosts[h].numSlots; s++)
            {
                if (hosts[h].slots[s].username) free(hosts[h].slots[s].username);
                hosts[h].slots[s].username = NULL;
                if (hosts[h].slots[s].filename) free(hosts[h].slots[s].filename);
                hosts[h].slots[s].filename = NULL;
                if (hosts[h].slots[s].logname) free(hosts[h].slots[s].logname);
                hosts[h].slots[s].logname = NULL;
            }
            if (hosts[h].slots) free(hosts[h].slots);
            hosts[h].slots = NULL;
        }
        free(hosts);
        hosts = NULL;
        numHosts = 0;
        pthread_mutex_unlock(&hostsMutex);
        sem_destroy(&shutdownSemaphore);
        fprintf(daemonLogFile, "%s : IPA Stopped.\n", writeTimestamp(timestamp, sizeof(timestamp)));
        free(highPriorityQueue);
        free(lowPriorityQueue);
        fclose(daemonLogFile);
        return 0;
    }
}

static int ReadTorqueNodesFile(char *torqueNodesFilename)
{   /* Look for what cpus are available on what machines */
    int h, np, totalSlots=0;
    char linebuf[128];
    char hostbuf[64];
    char timestamp[32];
    FILE *torqueNodesFile;
    int isFirstPass = (hosts == NULL);
    torqueNodesFile = fopen(torqueNodesFilename, "r");
    if (!torqueNodesFile)
    {
        if (errno == ENOENT)
        {
            fprintf(stderr, "Please copy a nodes file in torque format to %s\n", torqueNodesFilename);
            fprintf(daemonLogFile, "%s : Please copy a nodes file in torque format to %s.\n", writeTimestamp(timestamp, sizeof(timestamp)), torqueNodesFilename);
            return 1;
        }
        fprintf(stderr, "Cannot open %s : ", torqueNodesFilename);
        fprintf(daemonLogFile, "%s : Cannot open %s : ", writeTimestamp(timestamp, sizeof(timestamp)), torqueNodesFilename);
        perror("");
        return 1;
    }
    if (isFirstPass)
        fprintf(stdout, "Using Nodes:\n");
    else
        fprintf(daemonLogFile, "%s : Using Nodes:\n", writeTimestamp(timestamp, sizeof(timestamp)));
    pthread_mutex_lock(&hostsMutex);
    while (fgets(linebuf, sizeof(linebuf), torqueNodesFile))
    {
        int retval = sscanf(linebuf, "%63s np=%d", hostbuf, &np);
        if (retval == 2 || retval == 1)
        {
            if (retval == 1) np = 1;
            if (isFirstPass)
                fprintf(stdout, "%s np=%d\n", hostbuf, np);
            else
                fprintf(daemonLogFile, "%s : %s np=%d\n", writeTimestamp(timestamp, sizeof(timestamp)), hostbuf, np);
            totalSlots += np;
            for (h=0; h<numHosts; h++)
                if (hosts[h].type == LINHOST && hosts[h].name && !strcmp(hostbuf, hosts[h].name))
                    break;
            if (h == numHosts) /* This is a new host */
            {
                numHosts++;
                hosts = (Host *)realloc(hosts, numHosts * sizeof(Host));
                hosts[h].type = LINHOST;
                hosts[h].name = strdup(hostbuf);
                hosts[h].owner = NULL;
                hosts[h].isUp = 1;
                hosts[h].fd = -1;
                hosts[h].slots = (Slot *)calloc(np, sizeof(Slot));
                hosts[h].numSlots = np;
                if (!isFirstPass)
                    fprintf(daemonLogFile, "%s : This one is new.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            }
            else if (hosts[h].numSlots < np)
            {
                /* Number of slots grew */
                int oldNumSlots = hosts[h].numSlots;
                hosts[h].numSlots = np;
                hosts[h].slots = (Slot *)realloc(hosts[h].slots, hosts[h].numSlots * sizeof(Slot));
                memset(hosts[h].slots + oldNumSlots, 0, (hosts[h].numSlots - oldNumSlots) * sizeof(Slot));
                fprintf(daemonLogFile, "%s : This grew from %d\n", writeTimestamp(timestamp, sizeof(timestamp)), oldNumSlots);
            }
            else if (hosts[h].numSlots > np)
            {
                fprintf(daemonLogFile, "%s : Don't know how to shrink this from %d yet\n", writeTimestamp(timestamp, sizeof(timestamp)), hosts[h].numSlots);
            }
        }
    }
    /* TODO see what hosts were removed from the file and take them out */
    pthread_mutex_unlock(&hostsMutex);
    if (isFirstPass)
        fprintf(stdout, "Total : %d\n", totalSlots);
    else
        fprintf(daemonLogFile, "%s : Total : %d\n", writeTimestamp(timestamp, sizeof(timestamp)), totalSlots);
    fflush(stdout);
    fclose(torqueNodesFile);
    return 0;
}

static void *PingHosts(void *arg)
{
    char timestamp[32];
    while (1)
    {
        char **testHosts;
        int    numTestHosts = 0, h, t;
        pthread_mutex_lock(&hostsMutex); /* Since this test could take a while, we need to get a copy of the hosts
                                            to test in case it changes in the middle of the test */
        testHosts = (char **)malloc(numHosts * sizeof(char *)); /* Possibly bigger than we need */
        for (h=0; h<numHosts; h++)
            if (hosts[h].type == LINHOST && hosts[h].name)
            {
                testHosts[numTestHosts] = strdup(hosts[h].name);
                numTestHosts++;
            }
        pthread_mutex_unlock(&hostsMutex);
        for (t=0; t<numTestHosts; t++)
        {
            pid_t pid = fork();
            if (pid == 0)
            {
                setpgid(0,0); /* Put this process in its own group so we don't wait on it in WaitForJobs */
                execl("/bin/ping", "ping", "-c", "1", "-w", "1", testHosts[t], (char*)NULL);
                exit(42);
            } /* child process */
            else if (pid < 0)
            {
                perror_to_log_and_flush("Call to fork() failed");
            } /* fork error */
            else
            {
                int childExitStatus;
                pid_t waitResult;
                waitResult = waitpid(pid, &childExitStatus, 0);
                if (waitResult == -1)
                {
                    perror_to_log_and_flush("Call to waitpid() failed");
                }
                else if (!WIFEXITED(childExitStatus) || (WIFEXITED(childExitStatus) && WEXITSTATUS(childExitStatus)))
                {
                    pthread_mutex_lock(&hostsMutex);
                    for (h=0; h<numHosts; h++)
                        if (hosts[h].type == LINHOST && hosts[h].name && !strcmp(hosts[h].name, testHosts[t]) && hosts[h].isUp)
                        {
                            fprintf(daemonLogFile, "%s : Host %s is down\n", writeTimestamp(timestamp, sizeof(timestamp)), testHosts[t]);
                            fflush(daemonLogFile);
                            hosts[h].isUp = 0;
                            break;
                        }
                    pthread_mutex_unlock(&hostsMutex);
                }
                else if (WIFEXITED(childExitStatus) && WEXITSTATUS(childExitStatus) == 0)
                {
                    pthread_mutex_lock(&hostsMutex);
                    for (h=0; h<numHosts; h++)
                        if (hosts[h].type == LINHOST && hosts[h].name && !strcmp(hosts[h].name, testHosts[t]) && !hosts[h].isUp)
                        {
                            fprintf(daemonLogFile, "%s : Host %s is up\n", writeTimestamp(timestamp, sizeof(timestamp)), testHosts[t]);
                            fflush(daemonLogFile);
                            hosts[h].isUp = 1;
                            break;
                        }
                    pthread_mutex_unlock(&hostsMutex);
                }
            } /* parent process */
            free(testHosts[t]);
        } /* Loop over testHosts */
        free(testHosts);
        sleep(60);
    }
}

static void *WatchNodes(void *arg)
{
    char *torqueNodesFilename = (char *)arg;
    char timestamp[32];
    time_t curTime, lastTime = 0;
    char *nodesFilename = (char *)arg;
    char notifyEventBuf[sizeof(struct inotify_event) + FILENAME_MAX];
    int notifyFD, notifyReadLen;
    notifyFD = inotify_init();
    if (notifyFD == -1)
    {
        perror_to_log_and_flush("subthread inotify_init");
        sem_post(&shutdownSemaphore);
        return NULL;
    }
    if (inotify_add_watch(notifyFD, nodesFilename, IN_MODIFY) == -1)
    {
        perror_to_log_and_flush("subthread inotify_add_watch");
        sem_post(&shutdownSemaphore);
        return NULL;
    }
    while ((notifyReadLen = read(notifyFD, notifyEventBuf, sizeof(notifyEventBuf))) > 0 || errno == EINTR)
    {
        /* struct inotify_event *event = (struct inotify_event *)notifyEventBuf; */
        if (notifyReadLen <= 0) continue; /* EINTR happened */
        curTime = time(NULL);
        if (curTime != lastTime)
        {
            fprintf(daemonLogFile, "%s : %s edited.\n", writeTimestamp(timestamp, sizeof(timestamp)), torqueNodesFilename);
            if (ReadTorqueNodesFile(torqueNodesFilename))
                fprintf(daemonLogFile, "%s : Reread nodes file failed.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            else
                fprintf(daemonLogFile, "%s : Reread nodes file succeeded.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            fflush(daemonLogFile);
        }
        lastTime = curTime;
    }
    return NULL;
}

static void *ExecuteClusters(void *arg)
{
    int isHighPriority = arg ? 1 : 0;
    char timestamp[32];
    struct dirent **namelist;
    int numNames = scandir(queueDirName, &namelist, isHighPriority ? ClusterNumOfHighPriorityDirent : ClusterNumOfLowPriorityDirent, QueueDirCompare);
    if (numNames < 0)
    {
        perror_to_log_and_flush("scandir");
    }
    else
    {
        int n;
        for (n=0; n<numNames; n++) /* queue event for existing files in order */
        {
            char fullFilename[FILENAME_MAX];
            snprintf(fullFilename, FILENAME_MAX, "%s/%s", queueDirName, namelist[n]->d_name);
            Enqueue(isHighPriority, strdup(fullFilename));
            free(namelist[n]);
        }
        free(namelist);
    }
    while (1)
    {
        char    *line    = NULL;
        size_t   lineLen = 0;
        ssize_t  lineReadLen;
        char    *eventFileBasename, *eventFileFullname = Dequeue(isHighPriority);
        int      clusterNum;
        struct stat statBuf;
        if (!eventFileFullname)
        {
            if (isHighPriority)
            {
                pthread_mutex_lock(&priorityActiveMutex);
                priorityActive = 0;
                pthread_mutex_unlock(&priorityActiveMutex);
            }
            sleep(1);
            continue;
        }
        eventFileBasename = strrchr(eventFileFullname, '/');
        if (!eventFileBasename)
        {
            sleep(1);
            free(eventFileFullname);
            continue;
        }
        eventFileBasename++;
        if (isHighPriority)
            clusterNum = atoi(eventFileBasename+1);
        else
            clusterNum = atoi(eventFileBasename);
        if (!clusterNum)
        {
            sleep(1);
            free(eventFileFullname);
            continue;
        }
        if (stat(eventFileFullname, &statBuf) == 0)
        {
            int j, numJobs = 0;
            int *numCPUsInJob;
            int *hostsToRunJobs;
            int currentJob = 0;
            int notEnoughWindowsSlots;
            char logName[FILENAME_MAX];
            char execName[FILENAME_MAX];
            char *argumentString = NULL;
            char *environmentString = NULL;
            char outputName[FILENAME_MAX];
            char errorName[FILENAME_MAX];
            char userName[128];
            FILE *qFile;
            struct passwd *pwUnownedResult;
            pthread_mutex_lock(&getpwMutex);
            pwUnownedResult = getpwuid(statBuf.st_uid);
            if (pwUnownedResult)
                snprintf(userName, sizeof(userName), "%s", pwUnownedResult->pw_name);
            pthread_mutex_unlock(&getpwMutex);
            if (!pwUnownedResult)
            {
                perror_to_log_and_flush("getpwuid");
                free(eventFileFullname);
                continue;
            }
            qFile = fopen(eventFileFullname, "r");
            if (!qFile)
            {
                fprintf(daemonLogFile, "%s : Cannot open %s : ", writeTimestamp(timestamp, sizeof(timestamp)), eventFileFullname);
                perror_to_log_and_flush("");
                free(eventFileFullname);
                continue;
            }
            *logName = 0;
            *execName = 0;
            *outputName = 0;
            *errorName = 0;
            while ((lineReadLen = getline(&line, &lineLen, qFile)) != -1) /* Get the number of jobs in file and the log file name */
            {
                if (strncmp(line, "queue", 5) == 0)
                    numJobs++;
                else if (strncmp(line, "log = ", 6) == 0)
                    snprintf(logName, sizeof(logName), "%s", line+6);
            }
            { /* Strip the trailing newlines */
                char *c;
                c = strrchr(logName, '\n');
                if (c) *c = 0;
            }
            numCPUsInJob = (int *)malloc(numJobs * sizeof(int));
            if (!numCPUsInJob)
            {
                perror_to_log_and_flush("Out of Memory");
                sem_post(&shutdownSemaphore);
                return NULL;
            }
            else
            {
                for (j=0; j<numJobs; j++)
                    numCPUsInJob[j] = 1;
                rewind(qFile);
                j = 0;
                while ((lineReadLen = getline(&line, &lineLen, qFile)) != -1) /* Now get the requested cpus for each job */
                {
                    if (strncmp(line, "queue", 5) == 0)
                        j++;
                    else if (strncmp(line, "request_cpus = ", 15) == 0)
                        numCPUsInJob[j] = numCPUsInJob[j] < 0 ? -atoi(line + 15) : atoi(line + 15);
                    else if (strncmp(line, "Requirements = ( OpSys == \"WINDOWS\" )", 37) == 0)
                        numCPUsInJob[j] *= -1;  /* Windows jobs have the numcpus negative */
                }
            }
            if (!ClusterCanRunEventually(userName, numJobs, numCPUsInJob, &notEnoughWindowsSlots))
            {
                if (notEnoughWindowsSlots)
                    fprintf(daemonLogFile, "%s : Not enough windows slots online for submitted file %s\n", writeTimestamp(timestamp, sizeof(timestamp)), eventFileBasename);
                else
                    fprintf(daemonLogFile, "%s : CPU requirements won't fit in avaliable slots in submitted file %s\n", writeTimestamp(timestamp, sizeof(timestamp)), eventFileBasename);
                fflush(daemonLogFile);
                fclose(qFile);
                unlink(eventFileFullname);
                free(numCPUsInJob);
                if (line) free(line), line = NULL;
                free(eventFileFullname);
                continue;
            }
            rewind(qFile);
            if (isHighPriority)
            {
                pthread_mutex_lock(&priorityActiveMutex);
                priorityActive = 1;
                pthread_mutex_unlock(&priorityActiveMutex);
                while (!(hostsToRunJobs = HostsToRunJobs(userName, numJobs, numCPUsInJob, NULL)))
                {
                    sleep(1);
                }
            }
            else
            {
                int p;
                pthread_mutex_lock(&priorityActiveMutex);
                p = priorityActive;
                pthread_mutex_unlock(&priorityActiveMutex);
                while (p || !(hostsToRunJobs = HostsToRunJobs(userName, numJobs, numCPUsInJob, NULL)))
                {
                    sleep(1);
                    pthread_mutex_lock(&priorityActiveMutex);
                    p = priorityActive;
                    pthread_mutex_unlock(&priorityActiveMutex);
                }
            }
            if (stat(eventFileFullname, &statBuf) != 0) /* Check to see if file is still there */
            {
                free(hostsToRunJobs);
                fclose(qFile);
                free(numCPUsInJob);
                if (line) free(line), line = NULL;
                free(eventFileFullname);
                continue;
            }
            pthread_mutex_lock(&jobsStartingMutex);
            fprintf(daemonLogFile, "%s : Starting cluster %d with %d jobs for %s%s\n", writeTimestamp(timestamp, sizeof(timestamp)), clusterNum, numJobs, userName, isHighPriority ? " (High Priority)" : "");
            for (j=0; j<numJobs; j++)
            {
                int s, restartCount = 0;
                int hostnum = hostsToRunJobs[j];
                int cpusInJob = numCPUsInJob[j] > 0 ? numCPUsInJob[j] : -numCPUsInJob[j];
                pid_t pid;
                while ((lineReadLen = getline(&line, &lineLen, qFile)) != -1)
                {
                    char *c;
                    if (strncmp(line, "executable = ", 13) == 0)
                    {
                        snprintf(execName, sizeof(execName), "%s", line+13);
                        c = strrchr(execName, '\n');
                        if (c) *c = 0;
                    }
                    else if (strncmp(line, "arguments = \"", 13) == 0)
                    {
                        if (argumentString) free(argumentString);
                        argumentString = strdup(line+13);
                        c = strrchr(argumentString, '\"');
                        if (c) *c = 0;
                    }
                    else if (strncmp(line, "environment = \"", 15) == 0)
                    {
                        if (environmentString) free(environmentString);
                        environmentString = strdup(line+15);
                        c = strrchr(environmentString, '\"');
                        if (c) *c = 0;
                    }
                    else if (strncmp(line, "output = ", 9) == 0)
                    {
                        snprintf(outputName, sizeof(outputName), "%s", line+9);
                        c = strrchr(outputName, '\n');
                        if (c) *c = 0;
                    }
                    else if (strncmp(line, "error = ", 8) == 0)
                    {
                        snprintf(errorName, sizeof(errorName), "%s", line+8);
                        c = strrchr(errorName, '\n');
                        if (c) *c = 0;
                    }
                    else if (strncmp(line, "IPA_restart = ", 14) == 0)
                    {
                        restartCount = atoi(line + 14);
                    }
                    else if (strncmp(line, "queue", 5) == 0)
                    {
                        break;
                    }
                }
                {   /* Replace $$(OpSys).exe with LINUX.exe in executable name */
                    char *execSuffix = strstr(execName, "$$(OpSys).exe");
                    if (execSuffix)
                        strcpy(execSuffix, "LINUX.exe"); /* Safe because the string is shorter */
                }
                if (restartCount < 0) restartCount = 0;
                if (restartCount > 9) restartCount = 9;
                if (hosts[hostnum].type == LINHOST)
                {
                    fflush(NULL);  /* Flush before fork to keep from getting multiple lines in the logfile */
                    usleep(5000); /* Pause just a bit for each fork */
                    pid = fork();
                    if (pid == 0)
                    {   /* In child */
                        if (activeUID == 0 && (setgid(statBuf.st_gid) != 0 || setuid(statBuf.st_uid) != 0))
                        {
                            perror_to_log_and_flush("Could not setuid to owner of submission file");
                            exit(1);
                        }
                        else
                        {
                            if (*logName)
                            {   /* Open output to user's log file */
                                FILE *userLog = fopen(logName, "a");
                                if (!userLog)
                                {
                                    fprintf(daemonLogFile, "%s : Could not open %s for appending : ", writeTimestamp(timestamp, sizeof(timestamp)), logName);
                                    perror_to_log_and_flush("");
                                }
                                else
                                {
                                    char datestamp[16];
                                    fprintf(userLog, "014 (%d.000.%03d) %s %s Node %d executing on host: %s\n",
                                            clusterNum, currentJob,
                                            writeCondorDatestamp(datestamp, sizeof(datestamp)),
                                            writeCondorTimestamp(timestamp, sizeof(timestamp)),
                                            currentJob,
                                            hosts[hostnum].name);
                                    fclose(userLog);
                                }
                            }
                            if (*outputName)
                            {   /* Send stdout to user's file */
                                FILE *userOut = fopen(outputName, "a");
                                if (!userOut)
                                {
                                    fprintf(daemonLogFile, "%s : Could not open %s for appending : ", writeTimestamp(timestamp, sizeof(timestamp)), outputName);
                                    perror_to_log_and_flush("");
                                }
                                else
                                {
                                    dup2(fileno(userOut), STDOUT_FILENO);
                                    fclose(userOut);
                                }
                            }
                            if (*errorName)
                            {   /* Send stdout to user's file */
                                FILE *userErr = fopen(errorName, "a");
                                if (!userErr)
                                {
                                    fprintf(daemonLogFile, "%s : Could not open %s for appending : ", writeTimestamp(timestamp, sizeof(timestamp)), errorName);
                                    perror_to_log_and_flush("");
                                }
                                else
                                {
                                    dup2(fileno(userErr), STDERR_FILENO);
                                    fclose(userErr);
                                }
                            }
                            /* Do not close daemon log file before these execs.  That way IPA cannot be started again until these processes finish since they inherit the lock */
                            /* That also means that these processes will be killed with fuser -k on the log file which is used in the init.d file */
                            if (environmentString)
                            {
                                if (argumentString)
                                    execl("/usr/bin/ssh", "ssh", "-tt", "-o BatchMode=yes", hosts[hostnum].name, IPA_QUEUE_DIR, environmentString, execName, argumentString, (char*)NULL);
                                else
                                    execl("/usr/bin/ssh", "ssh", "-tt", "-o BatchMode=yes", hosts[hostnum].name, IPA_QUEUE_DIR, environmentString, execName, (char*)NULL);
                            }
                            else
                            {
                                if (argumentString)
                                    execl("/usr/bin/ssh", "ssh", "-tt", "-o BatchMode=yes", hosts[hostnum].name, IPA_QUEUE_DIR, execName, argumentString, (char*)NULL);
                                else
                                    execl("/usr/bin/ssh", "ssh", "-tt", "-o BatchMode=yes", hosts[hostnum].name, IPA_QUEUE_DIR, execName, (char*)NULL);
                            }
                            perror_to_log_and_flush("Call to exec() failed");
                            exit(42);
                        }
                    }
                    else if (pid == -1)
                    {
                        perror_to_log_and_flush("sub thread fork");
                        sem_post(&shutdownSemaphore);
                        return NULL;
                    }
                } /* End run job on linux */
                else if (hosts[hostnum].type == WINHOST)
                {
                    IPApacket ipaPacket;
                    ipaPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
                    ipaPacket.packetType = IPApacketType_submit;
                    ipaPacket.flags = clusterNum;
                    ipaPacket.subflags = currentJob;
                    pid = 1;
                    memset(ipaPacket.buf, 0, sizeof(ipaPacket.buf));
                    if (argumentString)
                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "%s %s", "IsfDriver.Windows.exe", argumentString);
                    else
                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "%s", "IsfDriver.Windows.exe");
                    write(hosts[hostnum].fd, &ipaPacket, sizeof(ipaPacket));
                    if (*logName && stat(logName, &statBuf) == 0) /* Make sure it exists.  Don't want to create it as root */
                    {   /* Open output to user's log file */
                        FILE *userLog = fopen(logName, "a");
                        if (!userLog)
                        {
                            fprintf(daemonLogFile, "%s : Could not open %s for appending : ", writeTimestamp(timestamp, sizeof(timestamp)), logName);
                            perror_to_log_and_flush("");
                        }
                        else
                        {
                            char datestamp[16];
                            fprintf(userLog, "014 (%d.000.%03d) %s %s Node %d executing on host: %s\n",
                                    clusterNum, currentJob,
                                    writeCondorDatestamp(datestamp, sizeof(datestamp)),
                                    writeCondorTimestamp(timestamp, sizeof(timestamp)),
                                    currentJob,
                                    hosts[hostnum].name);
                            fclose(userLog);
                        }
                    }
                }
                else
                {
                    fprintf(daemonLogFile, "%s : Host type for %s not set", writeTimestamp(timestamp, sizeof(timestamp)), hosts[hostnum].name);
                    pid = 0;
                }
                pthread_mutex_lock(&hostsMutex);
                for (s=0; s<hosts[hostnum].numSlots && cpusInJob && pid; s++)
                {
                    if (hosts[hostnum].slots[s].pid)
                        continue;
                    cpusInJob--;
                    hosts[hostnum].slots[s].filename = strdup(eventFileFullname);
                    hosts[hostnum].slots[s].username = strdup(userName);
                    hosts[hostnum].slots[s].logname = strdup(logName);
                    if (!hosts[hostnum].slots[s].filename || !hosts[hostnum].slots[s].username || !hosts[hostnum].slots[s].logname)
                    {
                        fprintf(daemonLogFile, "%s : Out of memory\n", writeTimestamp(timestamp, sizeof(timestamp)));
                        if (hosts[hostnum].slots[s].filename) free(hosts[hostnum].slots[s].filename);
                        if (hosts[hostnum].slots[s].username) free(hosts[hostnum].slots[s].username);
                        if (hosts[hostnum].slots[s].logname) free(hosts[hostnum].slots[s].logname);
                        hosts[hostnum].slots[s].filename = hosts[hostnum].slots[s].username = hosts[hostnum].slots[s].logname = NULL;
                    }
                    else
                    {
                        hosts[hostnum].slots[s].pid = pid;
                        hosts[hostnum].slots[s].uid = statBuf.st_uid;
                        hosts[hostnum].slots[s].clusterNum = clusterNum;
                        hosts[hostnum].slots[s].jobNum = currentJob;
                        hosts[hostnum].slots[s].restartCount = restartCount;
                    }
                }
                pthread_mutex_unlock(&hostsMutex);
                if (cpusInJob)
                {
                    fprintf(daemonLogFile, "%s : Out of slots\n", writeTimestamp(timestamp, sizeof(timestamp)));
                }
                currentJob++;
            } /* Loop over jobs */
            fflush(daemonLogFile);
            pthread_mutex_unlock(&jobsStartingMutex);
            free(numCPUsInJob);
            free(hostsToRunJobs);
            if (environmentString) free(environmentString);
            if (argumentString) free(argumentString);
            fclose(qFile);
        } /* Stat on file succeeds */
        if (line) free(line);
        line = NULL;
        free(eventFileFullname);
    }
    sem_post(&shutdownSemaphore);
    return NULL;
}

static void RewriteSubmission(char *filename, int restartCount)
{
    int isHighPriority = 0;
    char timestamp[32], *filenameBase;
    FILE *subFile = fopen(filename, "r+");
    if (restartCount < 0) restartCount = 0;
    if (restartCount > 9) restartCount = 9;
    filenameBase = strrchr(filename, '/');
    if (filenameBase)
        isHighPriority = filenameBase[1] == '_';
    if (subFile)
    {
        char lineBuf[1024];
        long filePos;
        while ((filePos = ftell(subFile)), fgets(lineBuf, sizeof(lineBuf), subFile))
        {
            if (strncmp(lineBuf, "IPA_restart = ", 14) == 0)
            {
                if (fseek(subFile, filePos + 14, SEEK_SET) == -1)
                {
                    fprintf(daemonLogFile, "%s : Could not seek in %s for editing : ", writeTimestamp(timestamp, sizeof(timestamp)), filename);
                    perror_to_log_and_flush("");
                }
                else
                {
                    fprintf(subFile, "%d", restartCount);
                }
            }
        }
        fclose(subFile);
        Enqueue(isHighPriority, strdup(filename));
    }
    else
    {
        fprintf(daemonLogFile, "%s : Could not open %s for editing : ", writeTimestamp(timestamp, sizeof(timestamp)), filename);
        perror_to_log_and_flush("");
    }
}

static void *WaitOnJobs(void *arg)
{
    char  timestamp[32];
    while (1)
    {
        int status;
        int waitResult;
        waitResult = waitpid(0, &status, 0); /* Wait on processes in this process group */
        if (waitResult == -1)
        {
            if (errno != ECHILD)
                perror_to_log_and_flush("");
            sleep(1); /* If there are no children, waitpid will not hang.  Sleep to avoid the tight loop */
            continue;
        }
        pthread_mutex_lock(&jobsStartingMutex); /* Don't want to process any exits while a cluster is being submitted */
        if (waitResult > 0)
        {
            if (!WIFSTOPPED(status) && !WIFCONTINUED(status))
            {
                Slot *slot = NULL;
                int h, s;
                /* Find which slot had the childPid which died */
                pthread_mutex_lock(&hostsMutex);
                for (h=0; h<numHosts && !slot; h++)
                    for (s=0; s<hosts[h].numSlots && !slot; s++)
                        if (hosts[h].slots[s].pid == waitResult)
                            slot = hosts[h].slots + s;
                if (slot)
                {
                    FILE *logfile = NULL;
                    char  datestamp[16];
                    int   otherJobsInCluster = 0;
                    int   clusterNum = slot->clusterNum;
                    int   jobNum = slot->jobNum;
                    struct stat statBuf;
                    if (slot->logname[0] && stat(slot->logname, &statBuf) == 0) /* Make sure it exists.  Don't want to create it as root */
                    {
                        logfile = fopen(slot->logname, "a");
                        if (!logfile)
                        {
                            fprintf(daemonLogFile, "%s : Could not open %s for appending : ", writeTimestamp(timestamp, sizeof(timestamp)), slot->logname);
                            perror_to_log_and_flush("");
                        }
                    }
                    if (logfile)
                    {
                        fprintf(logfile, "015 (%d.000.%03d) %15s %15s Node %d terminated.\n", 
                                slot->clusterNum, slot->jobNum,
                                writeCondorDatestamp(datestamp, sizeof(datestamp)),
                                writeCondorTimestamp(timestamp, sizeof(timestamp)),
                                slot->jobNum);
                        if (WIFEXITED(status))
                        {
                            fprintf(logfile, "  (1) Normal termination (return value %d)\n", WEXITSTATUS(status));
                        }
                        else if (WIFSIGNALED(status))
                        {
                            fprintf(logfile, "  (0) Abnormal termination (signal %d)\n", WTERMSIG(status));
                        }
                        else /* Don't know what will cause it to get here, because we covered every case in the waitpid man page */
                        {
                            fprintf(logfile, "  (0) Abnormal termination (signal %d)\n", 254);
                            fprintf(daemonLogFile, "%s : %d.%d exited weirdly\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->clusterNum, slot->jobNum);
                        }
                    }
                    for (h=0; h<numHosts && !otherJobsInCluster; h++)
                        for (s=0; s<hosts[h].numSlots && !otherJobsInCluster; s++)
                            if (hosts[h].slots + s != slot && hosts[h].slots[s].clusterNum == slot->clusterNum && hosts[h].slots[s].jobNum != slot->jobNum)
                                otherJobsInCluster = 1;
                    if ((WIFSIGNALED(status) || (WIFEXITED(status) && WEXITSTATUS(status))) && slot->restartCount > 0)
                    {
                        fprintf(daemonLogFile, "%s : Head job for %s's cluster %d failed.\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->username, slot->clusterNum);
                        if (otherJobsInCluster)
                        {   /* Make the resubmit happen on the last jobs death. */
                            fprintf(daemonLogFile, "%s : Killing the rest of %s's cluster %d.\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->username, slot->clusterNum);
                            for (h=0; h<numHosts; h++)
                                for (s=0; s<hosts[h].numSlots; s++)
                                    if (hosts[h].slots + s != slot && hosts[h].slots[s].clusterNum == slot->clusterNum)
                                    {
                                        if (hosts[h].slots[s].pid > 1)
                                            kill(hosts[h].slots[s].pid, SIGTERM);
                                        else
                                        {
                                            IPApacket killPacket;
                                            memset(&killPacket, 0, sizeof(IPApacket));
                                            killPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
                                            killPacket.packetType  = IPApacketType_rm;
                                            killPacket.flags       = hosts[h].slots[s].clusterNum;
                                            killPacket.subflags    = hosts[h].slots[s].jobNum;
                                            write(hosts[h].fd, &killPacket, sizeof(killPacket));
                                        }
                                        hosts[h].slots[s].restartCount = -slot->restartCount;
                                    }
                        }
                        else
                        {
                            fprintf(daemonLogFile, "%s : Cluster %d requeued for %s\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->clusterNum, slot->username);
                            RewriteSubmission(slot->filename, slot->restartCount - 1);
                        }
                    }
                    else if (slot->restartCount < 0)
                    {
                        if (!otherJobsInCluster)
                        {
                            fprintf(daemonLogFile, "%s : Cluster %d requeued for %s\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->clusterNum, slot->username);
                            RewriteSubmission(slot->filename, -slot->restartCount - 1);
                        }
                    }
                    else if (!otherJobsInCluster)
                    {
                        if (logfile)
                            fprintf(logfile, "005 (%d.000.000) %15s %15s Job terminated.\n", 
                                    slot->clusterNum,
                                    writeCondorDatestamp(datestamp, sizeof(datestamp)),
                                    writeCondorTimestamp(timestamp, sizeof(timestamp)));
                        fprintf(daemonLogFile, "%s : Cluster %d finished for %s\n", writeTimestamp(timestamp, sizeof(timestamp)), clusterNum, slot->username);
                        unlink(slot->filename);
                    }
                    for (h=0; h<numHosts; h++)
                        for (s=0; s<hosts[h].numSlots; s++) /* Setting stuff to zero here, so use clusterNum and jobNum from above and not slot->jobNum or slot->clusterNum */
                            if (hosts[h].slots[s].clusterNum == clusterNum && hosts[h].slots[s].jobNum == jobNum)
                            {
                                hosts[h].slots[s].pid = 0;
                                hosts[h].slots[s].uid = 0;
                                if (hosts[h].slots[s].filename) free(hosts[h].slots[s].filename);
                                hosts[h].slots[s].filename = NULL;
                                hosts[h].slots[s].clusterNum = 0;
                                hosts[h].slots[s].jobNum = 0;
                                if (hosts[h].slots[s].username) free(hosts[h].slots[s].username);
                                hosts[h].slots[s].username = NULL;
                                if (hosts[h].slots[s].logname) free(hosts[h].slots[s].logname);
                                hosts[h].slots[s].logname = NULL;
                            }
                    if (logfile) fclose(logfile);
                }
                else /* slot is NULL because the pid that exited was not in the slots table */
                    fprintf(daemonLogFile, "%s : Child %d died that wasn't in a slot\n", writeTimestamp(timestamp, sizeof(timestamp)), waitResult);
                pthread_mutex_unlock(&hostsMutex);
                fflush(daemonLogFile);
            }
        }
        pthread_mutex_unlock(&jobsStartingMutex);
    }
    sem_post(&shutdownSemaphore);
    return NULL;
}

static void SendStatusToFD(int connectionFD, IPApacket *ipaPacket)
{
    int h, s;
    snprintf(ipaPacket->buf, sizeof(ipaPacket->buf), "%16s %4s %7s %6s %8s.%3s %s", "Hostname", "Slot", "Type", "Status", "Cluster", "Job", "User");
    write(connectionFD, ipaPacket, sizeof(IPApacket));
    pthread_mutex_lock(&hostsMutex);
    for (h=0; h<numHosts; h++)
        if (hosts[h].type)
            for (s=0; s<hosts[h].numSlots; s++)
            {
                Slot *slot = hosts[h].slots + s;
                if (slot->clusterNum)
                    snprintf(ipaPacket->buf, sizeof(ipaPacket->buf), "%16s %4d %7s %6s %8d.%03d %s",
                             s ? "\"" : hosts[h].name, s+1, hosts[h].type, hosts[h].isUp ? "Up" : "Down", slot->clusterNum, slot->jobNum, slot->username);
                else
                    snprintf(ipaPacket->buf, sizeof(ipaPacket->buf), "%16s %4d %7s %6s %8s",
                             s ? "\"" : hosts[h].name, s+1, hosts[h].type, hosts[h].isUp ? "Up" : "Down", "Free");
                write(connectionFD, ipaPacket, sizeof(IPApacket));
            }
    pthread_mutex_unlock(&hostsMutex);
}

static void SendQueueToFD(int connectionFD, IPApacket *ipaPacket)
{
    struct dirent **namelist;
    int h, s, numInQueue = 0;
    int numNames = scandir(queueDirName, &namelist, ClusterNumOfAnyPriorityDirent, QueueDirCompare);
    if (numNames < 0)
    {
        perror_to_log_and_flush("scandir");
    }
    else
    {
        int n;
        pthread_mutex_lock(&hostsMutex);
        for (n=0; n<numNames; n++)
        {
            int clusterNum = ClusterNumOfAnyPriorityDirent(namelist[n]);
            struct stat statBuf;
            char fullFilename[FILENAME_MAX];
            char priorityChar = namelist[n]->d_name[0] == '_' ? '^' : ' ';
            snprintf(fullFilename, FILENAME_MAX, "%s/%s", queueDirName, namelist[n]->d_name);
            if (stat(fullFilename, &statBuf) == 0)
            {
                char userName[128];
                struct passwd *pwUnownedResult;
                pthread_mutex_lock(&getpwMutex);
                pwUnownedResult = getpwuid(statBuf.st_uid);
                if (pwUnownedResult)
                    snprintf(userName, sizeof(userName), "%s", pwUnownedResult->pw_name);
                pthread_mutex_unlock(&getpwMutex);
                if (!pwUnownedResult)
                {
                    perror_to_log_and_flush("getpwuid");
                }
                else
                {
                    char *subName = namelist[n]->d_name;
                    int isRunning = 0;
                    if (*subName == '_') subName++;
                    subName = strchr(subName, '_');
                    if (subName)
                        subName++;
                    else
                        subName = namelist[n]->d_name;
                    for (h=0; h<numHosts && !isRunning; h++) /* Might be good to just get a list of clusters running ahead of time and check against that */
                        for (s=0; s<hosts[h].numSlots && !isRunning; s++)
                            if (hosts[h].slots[s].clusterNum == clusterNum)
                                isRunning = 1;
                    snprintf(ipaPacket->buf, sizeof(ipaPacket->buf), "%s %6d%c %12s %s", isRunning ? "Running" : "       ", clusterNum, priorityChar, userName, subName);
                    write(connectionFD, ipaPacket, sizeof(IPApacket));
                    numInQueue++;
                }
            }
            free(namelist[n]);
        }
        pthread_mutex_unlock(&hostsMutex);
        free(namelist);
    }
    snprintf(ipaPacket->buf, sizeof(ipaPacket->buf), "Total : %d Clusters", numInQueue);
    write(connectionFD, ipaPacket, sizeof(IPApacket));
}

static void *StatusServerSubthread(void *arg)
{
    char timestamp[32];
    int connectionFD = *(int *)arg;
    IPApacket ipaPacket;
    int bytesReceived = read(connectionFD, &ipaPacket, sizeof(ipaPacket));
    free(arg);
    if (bytesReceived == -1)
    {
        perror_to_log_and_flush("read");
    }
    else
    {
        if (bytesReceived != sizeof(ipaPacket))
        {
            fprintf(daemonLogFile, "%s : Truncated read got %d bytes and expected %lu bytes\n", writeTimestamp(timestamp, sizeof(timestamp)), bytesReceived, sizeof(ipaPacket));
            ipaPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
            ipaPacket.packetType = IPApacketType_none;
            ipaPacket.flags = 1;
            ipaPacket.subflags = 0;
            memset(ipaPacket.buf, 0, sizeof(ipaPacket.buf));
        }
        else
        {
            int h, s;
            struct ucred creds;
            socklen_t credslen = sizeof(creds);
            if (getsockopt(connectionFD, SOL_SOCKET, SO_PEERCRED, &creds, &credslen) == -1)
            {
                perror_to_log_and_flush("getsockopt on SO_PEERCRED");
            }
            else
            {
                if (ipaPacket.magicNumber != IPA_PACKET_MAGIC_NUMBER)
                {
                    ipaPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
                    ipaPacket.flags = 1;
                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Bad data sent to server");
                    write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                }
                else if (ipaPacket.packetType == IPApacketType_q)
                {
                    SendQueueToFD(connectionFD, &ipaPacket);
                }
                else if (ipaPacket.packetType == IPApacketType_status)
                {
                    SendStatusToFD(connectionFD, &ipaPacket);
                }
                else if (ipaPacket.packetType == IPApacketType_rm)
                {
                    char userName[128];
                    struct passwd *pwUnownedResult;
                    int clusterToRemove = atoi(ipaPacket.buf);
                    int softRemove = ipaPacket.flags;
                    int doRemove = 1;
                    ipaPacket.flags = 1;
                    pthread_mutex_lock(&getpwMutex);
                    pwUnownedResult = getpwuid(creds.uid);
                    if (pwUnownedResult)
                        snprintf(userName, sizeof(userName), "%s", pwUnownedResult->pw_name);
                    pthread_mutex_unlock(&getpwMutex);
                    if (!pwUnownedResult)
                    {
                        perror_to_log_and_flush("getpwuid");
                        doRemove = 0;
                    }
                    else if (!clusterToRemove && strncasecmp(ipaPacket.buf, "all", 3))
                    {   /* clusterToRemove is a string and it is not "all", so see if it is a user */
                        uid_t uidOfBuf;
                        pthread_mutex_lock(&getpwMutex);
                        pwUnownedResult = getpwnam(ipaPacket.buf);
                        if (pwUnownedResult)
                            uidOfBuf = pwUnownedResult->pw_uid;
                        pthread_mutex_unlock(&getpwMutex);
                        if (!pwUnownedResult)
                        {
                            snprintf(ipaPacket.buf + strlen(ipaPacket.buf), sizeof(ipaPacket.buf) - strlen(ipaPacket.buf), " : No such user");
                            write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                            doRemove = 0;
                        }
                        else if (creds.uid != 0 && creds.uid != uidOfBuf)
                        {
                            snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Only root can do that");
                            write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                            doRemove = 0;
                        }
                        else
                        {
                            creds.uid = uidOfBuf;
                        }
                    }
                    if (doRemove)
                    {
                        struct dirent **namelist;
                        int numRemoved = 0;
                        int numNames = scandir(queueDirName, &namelist, ClusterNumOfAnyPriorityDirent, QueueDirCompare);
                        if (numNames < 0)
                        {
                            perror_to_log_and_flush("scandir");
                        }
                        else
                        {
                            int n;
                            for (n=0; n<numNames; n++)
                            {
                                int clusterNum = ClusterNumOfAnyPriorityDirent(namelist[n]);
                                if (clusterNum == clusterToRemove || clusterToRemove == 0)
                                {
                                    struct stat statBuf;
                                    char fullFilename[FILENAME_MAX];
                                    snprintf(fullFilename, FILENAME_MAX, "%s/%s", queueDirName, namelist[n]->d_name);
                                    if (stat(fullFilename, &statBuf) == 0)
                                    {
                                        if (creds.uid == 0 || creds.uid == statBuf.st_uid)
                                        {
                                            int clusterIsRunning = 0;
                                            fprintf(daemonLogFile, "%s : %s removing cluster %d\n", writeTimestamp(timestamp, sizeof(timestamp)), userName, clusterNum);
                                            pthread_mutex_lock(&hostsMutex);
                                            for (h=0; h<numHosts && !clusterIsRunning; h++)
                                                for (s=0; s<hosts[h].numSlots && !clusterIsRunning; s++)
                                                    if (hosts[h].slots[s].clusterNum == clusterNum)
                                                        clusterIsRunning = 1;
                                            pthread_mutex_unlock(&hostsMutex);
                                            /* If this cluster is running, don't delete the file.  WaitForJobs will do it if needed */
                                            if (!clusterIsRunning)
                                                unlink(fullFilename);
                                            numRemoved++;
                                        }
                                    }
                                }
                                free(namelist[n]);
                            }
                            free(namelist);
                        }
                        pthread_mutex_lock(&hostsMutex);
                        for (h=0; h<numHosts; h++)
                            for (s=0; s<hosts[h].numSlots; s++)
                                if (hosts[h].slots[s].pid && (hosts[h].slots[s].clusterNum == clusterToRemove || clusterToRemove == 0))
                                    if (creds.uid == 0 || creds.uid == hosts[h].slots[s].uid)
                                    {
                                        fprintf(daemonLogFile, "%s : %s removing job %d.%d\n", writeTimestamp(timestamp, sizeof(timestamp)), userName, hosts[h].slots[s].clusterNum, hosts[h].slots[s].jobNum);
                                        if (!softRemove)
                                            hosts[h].slots[s].restartCount = 0; /* This keeps rm'd things from restarting */
                                        if (hosts[h].slots[s].pid > 1)
                                            kill(hosts[h].slots[s].pid, SIGTERM);
                                        else
                                        {
                                            IPApacket killPacket;
                                            memset(&killPacket, 0, sizeof(IPApacket));
                                            killPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
                                            killPacket.packetType  = IPApacketType_rm;
                                            killPacket.flags       = hosts[h].slots[s].clusterNum;
                                            killPacket.subflags    = hosts[h].slots[s].jobNum;
                                            write(hosts[h].fd, &killPacket, sizeof(killPacket));
                                        }
                                    }
                        pthread_mutex_unlock(&hostsMutex);
                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "%d cluster%s removed.", numRemoved, numRemoved == 1 ? "" : "s");
                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                    }
                    else
                    {
                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Error trying to remove clusters.");
                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                    }
                }
                else if (ipaPacket.packetType == IPApacketType_submit)
                {
                    char *incomingFile = strdup(ipaPacket.buf);
                    int isHighPriority = ipaPacket.flags;
                    ipaPacket.flags = 1; /* Default return is error condition.  It is set to zero on success below */
                    if (!incomingFile)
                    {
                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Server out of memory.");
                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                    }
                    else
                    {
                        char *incomingFileBasename = strrchr(incomingFile, '/');
                        if (!incomingFileBasename)
                        {
                            snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Need absolute pathname for submission file.");
                            write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                        }
                        else
                        {
                            struct stat statBuf;
                            if (stat(incomingFile, &statBuf) != 0)
                            {
                                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Server cannot find submission file %s.", incomingFile);
                                write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                            }
                            else
                            {
                                char userName[128];
                                struct passwd *pwUnownedResult;
                                pthread_mutex_lock(&getpwMutex);
                                pwUnownedResult = getpwuid(statBuf.st_uid);
                                if (pwUnownedResult)
                                    snprintf(userName, sizeof(userName), "%s", pwUnownedResult->pw_name);
                                pthread_mutex_unlock(&getpwMutex);
                                if (!pwUnownedResult)
                                {
                                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Server cannot find the owner of the submission file %s.", incomingFile);
                                    write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                }
                                else if (creds.uid != 0 && creds.uid != statBuf.st_uid)
                                {
                                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "You are not the owner of the submission file %s.", incomingFile);
                                    write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                }
                                else
                                {
                                    FILE *qFile = fopen(incomingFile, "r");
                                    if (!qFile)
                                    {
                                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Server cannot open submission file %s.", incomingFile);
                                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                    }
                                    else
                                    {
                                        char lineBuf[1024];
                                        int numJobs = 0;
                                        int *numCPUsInJob;
                                        while (fgets(lineBuf, sizeof(lineBuf), qFile))
                                        {
                                            if (strncmp(lineBuf, "queue", 5) == 0)
                                                numJobs++;
                                        }
                                        numCPUsInJob = (int *)malloc(numJobs * sizeof(int));
                                        if (numCPUsInJob)
                                        {
                                            int j, notEnoughWindowsSlots;
                                            for (j=0; j<numJobs; j++)
                                                numCPUsInJob[j] = 1;
                                            rewind(qFile);
                                            j = 0;
                                            while (fgets(lineBuf, sizeof(lineBuf), qFile)) /* Now get the requested cpus for each job */
                                            {
                                                if (strncmp(lineBuf, "queue", 5) == 0)
                                                    j++;
                                                else if (strncmp(lineBuf, "request_cpus = ", 15) == 0)
                                                    numCPUsInJob[j] = numCPUsInJob[j] < 0 ? -atoi(lineBuf + 15) : atoi(lineBuf + 15);
                                                else if (strncmp(lineBuf, "Requirements = ( OpSys == \"WINDOWS\" )", 37) == 0)
                                                    numCPUsInJob[j] *= -1;  /* Windows jobs have the numcpus negative */
                                            }
                                            if (!ClusterCanRunEventually(userName, numJobs, numCPUsInJob, &notEnoughWindowsSlots))
                                            {
                                                if (notEnoughWindowsSlots)
                                                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Not enough windows slots for submitted file %s.", incomingFile);
                                                else
                                                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "More jobs than slots in submitted file %s.", incomingFile);
                                                write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                            }
                                            else
                                            {
                                                int myNextRunNumber;
                                                pid_t pid;
                                                char newFilename[FILENAME_MAX];
                                                incomingFileBasename++;
                                                pthread_mutex_lock(&nextRunNumberMutex);
                                                if (isHighPriority)
                                                    snprintf(newFilename, FILENAME_MAX, "%s/_%d_%s", queueDirName, nextRunNumber, incomingFileBasename);
                                                else
                                                    snprintf(newFilename, FILENAME_MAX, "%s/%d_%s", queueDirName, nextRunNumber, incomingFileBasename);
                                                myNextRunNumber = nextRunNumber; /* keep a local copy so we can unlock the mutex */
                                                nextRunNumber++;
                                                pthread_mutex_unlock(&nextRunNumberMutex);
                                                pid = fork();
                                                if (pid == 0)
                                                {
                                                    setpgid(0,0); /* Put this process in its own group so we don't wait on it in WaitForJobs */
                                                    fclose(daemonLogFile);
                                                    execl("/bin/cp", "cp", "--preserve=ownership", incomingFile, newFilename, (char *)0);
                                                    exit(42);
                                                }
                                                else if (pid < 0)
                                                {
                                                    perror_to_log_and_flush("Call to fork() failed");
                                                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Error Submitting job(s)....");
                                                    write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                                }
                                                else
                                                {
                                                    int childExitStatus;
                                                    pid_t waitResult = waitpid(pid, &childExitStatus, 0);
                                                    if (waitResult == -1 || !WIFEXITED(childExitStatus) || (WIFEXITED(childExitStatus) && WEXITSTATUS(childExitStatus)))
                                                    {
                                                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Error Submitting job(s)....");
                                                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                                    }
                                                    else
                                                    {
                                                        ipaPacket.flags = 0;
                                                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Submitting job(s)....");
                                                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "1 job(s) submitted to cluster %d.", myNextRunNumber);
                                                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                                        Enqueue(isHighPriority, strdup(newFilename));
                                                    }
                                                }
                                            }
                                            free(numCPUsInJob);
                                        }
                                        fclose(qFile);
                                    }
                                }
                            }
                        }
                        free(incomingFile);
                    }
                }
                else if (ipaPacket.packetType == IPApacketType_test)
                {
                    char  *execToTest = strdup(ipaPacket.buf);
                    char **testHosts;
                    int    numTestHosts = 0;
                    pthread_mutex_lock(&hostsMutex); /* Since this test could take a while, we need to get a copy of the hosts
                                                        to test in case it changes in the middle of the test */
                    testHosts = (char **)malloc(numHosts * sizeof(char *)); /* Possibly bigger than we need */
                    for (h=0; h<numHosts; h++)
                        if (hosts[h].type == LINHOST)
                        {
                            testHosts[numTestHosts] = strdup(hosts[h].name);
                            numTestHosts++;
                        }
                    pthread_mutex_unlock(&hostsMutex);
                    for (h=0; h<numTestHosts; h++)
                    {
                        pid_t pid = fork();
                        if (pid == 0)
                        {
                            setpgid(0,0); /* Put this process in its own group so we don't wait on it in WaitForJobs */
                            if (activeUID == 0 && (setgid(creds.gid) != 0 || setuid(creds.uid) != 0))
                            {
                                perror_to_log_and_flush("Could not setuid to owner of test");
                                exit(1);
                            }
                            fclose(daemonLogFile);
                            execl("/usr/bin/ssh", "ssh", "-tt", "-o BatchMode=yes", testHosts[h], execToTest, (char*)NULL);
                            exit(42);
                        } /* child process */
                        else if (pid < 0)
                        {
                            perror_to_log_and_flush("Call to fork() failed");
                            snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Server failed to fork a process.");
                            write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                        } /* fork error */
                        else
                        {
                            int childExitStatus;
                            pid_t waitResult;
                            snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Testing ssh connection to %s ...", testHosts[h]);
                            write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                            waitResult = waitpid(pid, &childExitStatus, 0);
                            if (waitResult == -1)
                            {
                                perror_to_log_and_flush("Call to waitpid() failed");
                                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Internal error waiting for child status.");
                            }
                            else if (!WIFEXITED(childExitStatus))
                            {
                                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Subprocess died on a signal.");
                            }
                            else if (WIFEXITED(childExitStatus) && WEXITSTATUS(childExitStatus))
                            {
                                char thisHostname[128];
                                if (gethostname(thisHostname, sizeof(thisHostname)) == -1)
                                {
                                    perror_to_log_and_flush("Call to gethostname() failed");
                                    snprintf(thisHostname, sizeof(thisHostname), "localhost");
                                }
                                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Failed with status %d.", WEXITSTATUS(childExitStatus));
                                write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                                if (WEXITSTATUS(childExitStatus) == 255)
                                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Check ssh keys and known_hosts setup by manually ssh'ing from %s to %s.", thisHostname, testHosts[h]);
                                else if (WEXITSTATUS(childExitStatus) == 127)
                                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Error running executable.  Try manually ssh'ing to %s and running %s.  It could be a path problem.", testHosts[h], execToTest);
                                else if (WEXITSTATUS(childExitStatus) < 127)
                                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Executable ran, but exited with a failure status.\nIt might run with the right command line arguments which are not passed in this test.");
                            }
                            else
                            {
                                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Passed.");
                            }
                            write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                        } /* parent process */
                        free(testHosts[h]);
                    } /* Loop over testHosts */
                    free(testHosts);
                    free(execToTest);
                } /* packet type test */
                else
                {
                    ipaPacket.flags = 1;
                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Unknown command sent to server");
                    write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                }
            }
        }
    }
    fflush(daemonLogFile);
    ipaPacket.packetType = IPApacketType_none;
    write(connectionFD, &ipaPacket, sizeof(ipaPacket));
    close(connectionFD);
    pthread_detach(pthread_self());
    return NULL;
}

static void *StatusServer(void *arg)
{
    struct sockaddr_un localAddress;
    socklen_t localAddressLength = sizeof(localAddress);
    char socketPath[UNIX_PATH_MAX];
    int connectionFD;
    int socketFD = socket(AF_UNIX, SOCK_STREAM, 0);
    if (socketFD < 0)
    {
        perror_to_log_and_flush("socket");
        sem_post(&shutdownSemaphore);
        return NULL;
    }
    snprintf(socketPath, UNIX_PATH_MAX, "%s/socket", queueDirName);
    unlink(socketPath);
    memset(&localAddress, 0, sizeof(localAddress));
    localAddress.sun_family = AF_UNIX;
    strcpy(localAddress.sun_path, socketPath);
    if (bind(socketFD, (struct sockaddr *)&localAddress, sizeof(localAddress)))
    {
        close(socketFD);
        perror_to_log_and_flush("bind");
        sem_post(&shutdownSemaphore);
        return NULL;
    }
    if (chmod(socketPath, 0777) == -1)
        perror_to_log_and_flush("chmod on server socket");
    if (listen(socketFD, 128) != 0)
    {
        close(socketFD);
        perror_to_log_and_flush("listen");
        sem_post(&shutdownSemaphore);
        return NULL;
    }
    while ((connectionFD = accept(socketFD, (struct sockaddr *)&localAddress, &localAddressLength)) > -1)
    {
        char timestamp[32];
        pthread_t subthread;
        int *connectionFDptr = (int *)malloc(sizeof (int));
        if (!connectionFDptr)
        {
            fprintf(daemonLogFile, "%s : Out of memory after connection accept.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            close(socketFD);
            sem_post(&shutdownSemaphore);
            return NULL;
        }
        *connectionFDptr = connectionFD;
        if (pthread_create(&subthread, NULL, StatusServerSubthread, connectionFDptr) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create subthread to handle server request.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            close(socketFD);
            sem_post(&shutdownSemaphore);
            return NULL;
        }
    }
    perror_to_log_and_flush("accept");
    close(socketFD);
    sem_post(&shutdownSemaphore);
    return NULL;
}

static void *WindowsServerSubthread(void *arg)
{
    char timestamp[32];
    int connectionFD = *(int *)arg;
    IPApacket ipaPacket;
    char *ipaPacketBytes = (char *)&ipaPacket;
    int s, h, currentPos = 0;
    int cpuCount = 0;
    free(arg);
    pthread_detach(pthread_self());
    while (1)
    {
        int bytesReceived = read(connectionFD, ipaPacketBytes + currentPos, sizeof(ipaPacket) - currentPos);
        if (bytesReceived < 1)
        {
            if (errno == EINTR)
                continue;
            else
                break;
        }
        currentPos += bytesReceived;
        if (currentPos == (int)sizeof(ipaPacket))
        {
            currentPos = 0;
            if (ipaPacket.magicNumber != IPA_PACKET_MAGIC_NUMBER)
            {
                ipaPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
                ipaPacket.packetType = IPApacketType_none;
                ipaPacket.flags = 1;
                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Bad data sent to server");
                write(connectionFD, &ipaPacket, sizeof(ipaPacket));
            }
            else if (ipaPacket.packetType == IPApacketType_q)
            {
                SendQueueToFD(connectionFD, &ipaPacket);
            }
            else if (ipaPacket.packetType == IPApacketType_status)
            {
                SendStatusToFD(connectionFD, &ipaPacket);
            }
            else if (ipaPacket.packetType == IPApacketType_windows)
            {
                if (ipaPacket.flags < 1 || ipaPacket.flags > 64)
                {
                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Windows clients cannot claim they have %d cores available.", ipaPacket.flags);
                    write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                }
                else
                {
                    if (!cpuCount) /* This client is establishing a new host to send jobs to */
                    {
                        char hostbuf[32];
                        struct sockaddr_in peerAddr;
                        socklen_t peerAddrLen = sizeof(peerAddr);
                        cpuCount = ipaPacket.flags;
                        getpeername(connectionFD, (struct sockaddr *)&peerAddr, &peerAddrLen);
                        inet_ntop(AF_INET, &(peerAddr.sin_addr), hostbuf, sizeof(hostbuf));
                        pthread_mutex_lock(&hostsMutex);
                        for (h=0; h<numHosts; h++) /* Look for a vacant spot */
                            if (!hosts[h].type)
                                break; /* h will point to a hosts entry ready to use */
                        if (h == numHosts) /* Did not find a vacant slot */
                        {
                            numHosts++;
                            hosts = (Host *)realloc(hosts, numHosts * sizeof(Host));
                        }
                        /* snprintf(hostbuf, sizeof(hostbuf), "w-%s", ipaPacket.buf); */
                        hosts[h].type = WINHOST;
                        hosts[h].name = strdup(hostbuf);
                        hosts[h].owner = strdup(ipaPacket.buf);
                        hosts[h].fd = connectionFD;
                        hosts[h].isUp = 1;
                        hosts[h].numSlots = cpuCount;
                        hosts[h].slots = (Slot *)calloc(cpuCount, sizeof(Slot));
                        pthread_mutex_unlock(&hostsMutex);
                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Hello, %s", hosts[h].owner);
                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                    }
                    else
                    {
                        snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "CPU count already established");
                        write(connectionFD, &ipaPacket, sizeof(ipaPacket));
                    }
                }
            }
            else if (ipaPacket.packetType == IPApacketType_windows_result && cpuCount)
            {
                Slot *slot = NULL;
                /* Find which slot had the childPid which died */
                pthread_mutex_lock(&hostsMutex);
                for (h=0; h<numHosts && !slot; h++)
                    if (hosts[h].type == WINHOST && hosts[h].fd == connectionFD)
                        for (s=0; s<hosts[h].numSlots && !slot; s++)
                            if (hosts[h].slots[s].clusterNum == ipaPacket.flags && hosts[h].slots[s].jobNum == ipaPacket.subflags)
                                slot = hosts[h].slots + s;
                if (slot)
                {
                    FILE *logfile = NULL;
                    char  datestamp[16];
                    int   otherJobsInCluster = 0;
                    int   clusterNum = slot->clusterNum;
                    int   jobNum = slot->jobNum;
                    struct stat statBuf;
                    if (slot->logname[0] && stat(slot->logname, &statBuf) == 0) /* Make sure it exists.  Don't want to create it as root */
                    {
                        logfile = fopen(slot->logname, "a");
                        if (!logfile)
                        {
                            fprintf(daemonLogFile, "%s : Could not open %s for appending : ", writeTimestamp(timestamp, sizeof(timestamp)), slot->logname);
                            perror_to_log_and_flush("");
                        }
                    }
                    if (logfile)
                    {
                        fprintf(logfile, "015 (%d.000.%03d) %15s %15s Node %d terminated.\n", 
                                slot->clusterNum, slot->jobNum,
                                writeCondorDatestamp(datestamp, sizeof(datestamp)),
                                writeCondorTimestamp(timestamp, sizeof(timestamp)),
                                slot->jobNum);
                        fprintf(logfile, "  (1) Normal termination (return value %s)\n", ipaPacket.buf);
                    }
                    for (h=0; h<numHosts && !otherJobsInCluster; h++)
                        for (s=0; s<hosts[h].numSlots && !otherJobsInCluster; s++)
                            if (hosts[h].slots + s != slot && hosts[h].slots[s].clusterNum == slot->clusterNum && hosts[h].slots[s].jobNum != slot->jobNum)
                                otherJobsInCluster = 1;
                    if (atoi(ipaPacket.buf) && slot->restartCount > 0)
                    {
                        fprintf(daemonLogFile, "%s : Head job for %s's cluster %d failed.\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->username, slot->clusterNum);
                        if (otherJobsInCluster)
                        {   /* Make the resubmit happen on the last jobs death. */
                            fprintf(daemonLogFile, "%s : Killing the rest of %s's cluster %d.\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->username, slot->clusterNum);
                            for (h=0; h<numHosts; h++)
                                for (s=0; s<hosts[h].numSlots; s++)
                                    if (hosts[h].slots + s != slot && hosts[h].slots[s].clusterNum == slot->clusterNum)
                                    {
                                        if (hosts[h].slots[s].pid > 1)
                                            kill(hosts[h].slots[s].pid, SIGTERM);
                                        else
                                        {
                                            IPApacket killPacket;
                                            memset(&killPacket, 0, sizeof(IPApacket));
                                            killPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
                                            killPacket.packetType  = IPApacketType_rm;
                                            killPacket.flags       = hosts[h].slots[s].clusterNum;
                                            killPacket.subflags    = hosts[h].slots[s].jobNum;
                                            write(hosts[h].fd, &killPacket, sizeof(killPacket));
                                        }
                                        hosts[h].slots[s].restartCount = -slot->restartCount;
                                    }
                        }
                        else
                        {
                            fprintf(daemonLogFile, "%s : Cluster %d requeued for %s\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->clusterNum, slot->username);
                            RewriteSubmission(slot->filename, slot->restartCount - 1);
                        }
                    }
                    else if (slot->restartCount < 0)
                    {
                        if (!otherJobsInCluster)
                        {
                            fprintf(daemonLogFile, "%s : Cluster %d requeued for %s\n", writeTimestamp(timestamp, sizeof(timestamp)), slot->clusterNum, slot->username);
                            RewriteSubmission(slot->filename, -slot->restartCount - 1);
                        }
                    }
                    else if (!otherJobsInCluster)
                    {
                        if (logfile)
                            fprintf(logfile, "005 (%d.000.000) %15s %15s Job terminated.\n", 
                                    slot->clusterNum,
                                    writeCondorDatestamp(datestamp, sizeof(datestamp)),
                                    writeCondorTimestamp(timestamp, sizeof(timestamp)));
                        fprintf(daemonLogFile, "%s : Cluster %d finished for %s\n", writeTimestamp(timestamp, sizeof(timestamp)), clusterNum, slot->username);
                        unlink(slot->filename);
                    }
                    for (h=0; h<numHosts; h++)
                        for (s=0; s<hosts[h].numSlots; s++) /* Setting stuff to zero here, so use clusterNum and jobNum from above and not slot->jobNum or slot->clusterNum */
                            if (hosts[h].slots[s].clusterNum == clusterNum && hosts[h].slots[s].jobNum == jobNum)
                            {
                                hosts[h].slots[s].pid = 0;
                                hosts[h].slots[s].uid = 0;
                                if (hosts[h].slots[s].filename) free(hosts[h].slots[s].filename);
                                hosts[h].slots[s].filename = NULL;
                                hosts[h].slots[s].clusterNum = 0;
                                hosts[h].slots[s].jobNum = 0;
                                if (hosts[h].slots[s].username) free(hosts[h].slots[s].username);
                                hosts[h].slots[s].username = NULL;
                                if (hosts[h].slots[s].logname) free(hosts[h].slots[s].logname);
                                hosts[h].slots[s].logname = NULL;
                            }
                    if (logfile) fclose(logfile);
                }
                else /* slot is NULL because the pid that exited was not in the slots table */
                    fprintf(daemonLogFile, "%s : Windows child %d.%d died that wasn't in a slot\n", writeTimestamp(timestamp, sizeof(timestamp)), ipaPacket.flags, ipaPacket.subflags);
                pthread_mutex_unlock(&hostsMutex);
                fflush(daemonLogFile);
            }
            else if (ipaPacket.packetType == IPApacketType_rm)
            {
                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Windows clients cannot remove clusters.");
                write(connectionFD, &ipaPacket, sizeof(ipaPacket));
            }
            else if (ipaPacket.packetType == IPApacketType_submit)
            {
                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Windows clients cannot submit clusters.");
                write(connectionFD, &ipaPacket, sizeof(ipaPacket));
            }
            else if (ipaPacket.packetType == IPApacketType_test)
            {
                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Windows clients cannot test ssh environment.");
                write(connectionFD, &ipaPacket, sizeof(ipaPacket));
            }
            else
            {
                ipaPacket.flags = 1;
                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "Unknown command sent to server");
                write(connectionFD, &ipaPacket, sizeof(ipaPacket));
            }
            ipaPacket.packetType = IPApacketType_none;
            write(connectionFD, &ipaPacket, sizeof(ipaPacket));
        } /* Read was right size */
        fflush(daemonLogFile);
    }
    close(connectionFD);
    if (cpuCount)
    {
        pthread_mutex_lock(&hostsMutex);
        for (h=0; h<numHosts; h++)
            if (hosts[h].type == WINHOST && hosts[h].fd == connectionFD)
                break;
        if (h < numHosts)
        {
            hosts[h].type = NULL;
            free(hosts[h].name);
            hosts[h].name = NULL;
            free(hosts[h].owner);
            hosts[h].owner = NULL;
            hosts[h].fd = -1;
            hosts[h].isUp = 0;
            for (s=0; s<hosts[h].numSlots; s++)
            {
                if (hosts[h].slots[s].username) free(hosts[h].slots[s].username);
                hosts[h].slots[s].username = NULL;
                if (hosts[h].slots[s].filename) free(hosts[h].slots[s].filename);
                hosts[h].slots[s].filename = NULL;
                if (hosts[h].slots[s].logname) free(hosts[h].slots[s].logname);
                hosts[h].slots[s].logname = NULL;
            }
            free(hosts[h].slots);
            hosts[h].slots = NULL;
            hosts[h].numSlots = 0;
        }
        pthread_mutex_unlock(&hostsMutex);
    }
    return NULL;
}

static void *WindowsServer(void *arg)
{
    struct sockaddr_in localAddress;
    socklen_t localAddressLength = sizeof(localAddress);
    int connectionFD;
    int socketFD = socket(AF_INET, SOCK_STREAM, 0);
    if (socketFD < 0)
    {
        perror_to_log_and_flush("socket");
        sem_post(&shutdownSemaphore);
        return NULL;
    }
    memset(&localAddress, 0, sizeof(localAddress));
    localAddress.sin_family = AF_INET;
    localAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    localAddress.sin_port = htons(9549);
    if (bind(socketFD, (struct sockaddr *)&localAddress, sizeof(localAddress)))
    {
        close(socketFD);
        perror_to_log_and_flush("bind");
        sem_post(&shutdownSemaphore);
        return NULL;
    }
    if (listen(socketFD, 128) != 0)
    {
        close(socketFD);
        perror_to_log_and_flush("listen");
        sem_post(&shutdownSemaphore);
        return NULL;
    }
    while ((connectionFD = accept(socketFD, (struct sockaddr *)&localAddress, &localAddressLength)) > -1)
    {
        char timestamp[32];
        pthread_t subthread;
        int *connectionFDptr = (int *)malloc(sizeof (int));
        if (!connectionFDptr)
        {
            fprintf(daemonLogFile, "%s : Out of memory after connection accept.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            close(socketFD);
            sem_post(&shutdownSemaphore);
            return NULL;
        }
        *connectionFDptr = connectionFD;
        if (pthread_create(&subthread, NULL, WindowsServerSubthread, connectionFDptr) != 0)
        {
            fprintf(daemonLogFile, "%s : Could not create subthread to handle server request.\n", writeTimestamp(timestamp, sizeof(timestamp)));
            close(socketFD);
            sem_post(&shutdownSemaphore);
            return NULL;
        }
    }
    perror_to_log_and_flush("accept");
    close(socketFD);
    sem_post(&shutdownSemaphore);
    return NULL;
}
