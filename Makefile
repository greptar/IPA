EXECS=IPA IPA_client
LINKS=IPA_q.exe IPA_status.exe IPA_rm.exe IPA_submit.exe IPA_test.exe
LDLIBS=-lpthread
CFLAGS=-Wall -Wextra -Wshadow -Wno-unused-parameter -g
CC=gcc
all : $(EXECS) $(LINKS)
clean :
	rm -f $(EXECS) $(LINKS)
$(LINKS) : IPA_client
	ln -f $< $@
