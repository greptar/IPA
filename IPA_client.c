#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

/* This should be in system header */
#ifndef UNIX_PATH_MAX
#define UNIX_PATH_MAX 108
#endif

#include "IPApacket.h"

int main(int argc, char **argv)
{
    char *mode_q, *mode_status, *mode_rm, *mode_submit, *mode_test;
    char *queueEnv = getenv("IPA_QUEUE_DIR");
    char *queueHost, *queueDir, *colonPos;
    char thisHostname[32];
    int socketFD;
    if (gethostname(thisHostname, sizeof(thisHostname)) == -1)
    {
        perror("gethostname");
        return 1;
    }
    mode_q = strstr(argv[0], "_q");
    mode_status = strstr(argv[0], "_status");
    mode_rm = strstr(argv[0], "_rm");
    mode_submit = strstr(argv[0], "_submit");
    mode_test = strstr(argv[0], "_test");
    if (!mode_q && !mode_status && !mode_rm && !mode_submit && !mode_test)
    {
        fprintf(stderr, "Bad executable name %s.  Can't determine function.\n", argv[0]);
        return 1;
    }
    if (!queueEnv)
    {
        fprintf(stderr, "Please set $IPA_QUEUE_DIR\n");
        return 1;
    }
    queueHost = strdup(queueEnv);
    colonPos = strchr(queueHost, ':');
    if (!colonPos)
    {
        fprintf(stderr, "$IPA_QUEUE_DIR should be in the format host:directory\n");
        return 1;
    }
    *colonPos = 0;
    queueDir = colonPos + 1;
    if (strcmp(thisHostname, queueHost))
    {
        char tempBuf[FILENAME_MAX];
        char proc[FILENAME_MAX];
        char *envVar = queueEnv - strlen("IPA_QUEUE_DIR=");
        char *pwdVar = getenv("PWD") - strlen("PWD=");
        snprintf(tempBuf, sizeof(tempBuf), "/proc/%d/exe", getpid());
        int ch = readlink(tempBuf, proc, sizeof(proc));
        proc[ch] = 0;
        if (getenv("DONE_SSH_ALREADY"))
        {
            fprintf(stderr, "Tried to ssh to %s but ended up on %s.  Aborting.\n", queueHost, thisHostname);
            return 1;
        }
        fprintf(stdout, "Sending request to %s ...\n", queueHost);
        if (argc == 1)
        {
            if (getenv("IPA_SOFTREMOVE"))
                execl("/usr/bin/ssh", "ssh", "-n", queueHost, pwdVar, envVar, "IPA_SOFTREMOVE=1", "DONE_SSH_ALREADY=TRUE", proc, (char*)NULL);
            else if (getenv("IPA_PRIORITY"))
                execl("/usr/bin/ssh", "ssh", "-n", queueHost, pwdVar, envVar, "IPA_PRIORITY=1", "DONE_SSH_ALREADY=TRUE", proc, (char*)NULL);
            else
                execl("/usr/bin/ssh", "ssh", "-n", queueHost, pwdVar, envVar, "DONE_SSH_ALREADY=TRUE", proc, (char*)NULL);
        }
        else if (argc == 2)
        {
            if (getenv("IPA_SOFTREMOVE"))
                execl("/usr/bin/ssh", "ssh", "-n", queueHost, pwdVar, envVar, "IPA_SOFTREMOVE=1", "DONE_SSH_ALREADY=TRUE", proc, argv[1], (char*)NULL);
            else if (getenv("IPA_PRIORITY"))
                execl("/usr/bin/ssh", "ssh", "-n", queueHost, pwdVar, envVar, "IPA_PRIORITY=1", "DONE_SSH_ALREADY=TRUE", proc, argv[1], (char*)NULL);
            else
                execl("/usr/bin/ssh", "ssh", "-n", queueHost, pwdVar, envVar, "DONE_SSH_ALREADY=TRUE", proc, argv[1], (char*)NULL);
        }
        else
        {
            fprintf(stderr, "Too many arguments.\n");
        }
        return 1;
    }
    /* Below here is done on the machine where IPA is running */
    socketFD = socket(AF_UNIX, SOCK_STREAM, 0);
    if (socketFD < 0)
    {
        perror("socket");
        return 1;
    }
    else
    {
        char socketPath[UNIX_PATH_MAX];
        struct sockaddr_un remoteAddress;
        int bytesReceived;
        IPApacket ipaPacket;
        int bytesSent;
        ipaPacket.magicNumber = IPA_PACKET_MAGIC_NUMBER;
        ipaPacket.flags = 0;
        if (mode_q)
        {
            ipaPacket.packetType = IPApacketType_q;
        }
        else if (mode_status)
        {
            ipaPacket.packetType = IPApacketType_status;
        }
        else if (mode_rm)
        {
            ipaPacket.packetType = IPApacketType_rm;
            ipaPacket.flags = getenv("IPA_SOFTREMOVE") ? 1 : 0;
            if (argc > 1)
            {
                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "%s", argv[1]);
            }
            else
            {
                fprintf(stderr, "Cluster to remove not specified\n");
                return 1;
            }
        }
        else if (mode_submit)
        {
            ipaPacket.packetType = IPApacketType_submit;
            ipaPacket.flags = getenv("IPA_PRIORITY") ? 1 : 0;
            if (argc > 1)
            {
                if (argv[1][0] == '/')
                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "%s", argv[1]);
                else
                    snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "%s/%s", getenv("PWD"), argv[1]);
            }
            else
            {
                fprintf(stderr, "File to submit not specified\n");
                return 1;
            }
        }
        else if (mode_test)
        {
            ipaPacket.packetType = IPApacketType_test;
            if (argc > 1)
                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "%s", argv[1]);
            else
                snprintf(ipaPacket.buf, sizeof(ipaPacket.buf), "true");
        }
        else
        {
            fprintf(stderr, "Bad executable name %s.  Can't determine function.\n", argv[0]);
            return 1;
        }
        snprintf(socketPath, UNIX_PATH_MAX, "%s/socket", queueDir);
        memset(&remoteAddress, 0, sizeof(remoteAddress));
        remoteAddress.sun_family = AF_UNIX;
        strcpy(remoteAddress.sun_path, socketPath);
        if (connect(socketFD, (struct sockaddr *)&remoteAddress, sizeof(remoteAddress)))
        {
            close(socketFD);
            perror("connect");
        }
        bytesSent = write(socketFD, &ipaPacket, sizeof(ipaPacket));
        if (bytesSent == -1)
        {
            fprintf(stderr, "It does not appear that IPA is running.\n");
        }
        else if (bytesSent != sizeof(ipaPacket))
        {
            fprintf(stderr, "truncated write\n");
        }
        else
        {
            while (1)
            {
                bytesReceived = read(socketFD, &ipaPacket, sizeof(ipaPacket));
                if (bytesReceived == sizeof(ipaPacket))
                {
                    if (ipaPacket.magicNumber != IPA_PACKET_MAGIC_NUMBER)
                    {
                        fprintf(stderr, "Bad data from server\n");
                        break;
                    }
                    if (ipaPacket.packetType == IPApacketType_none)
                    {
                        break;
                    }
                    else
                    {
                        if (ipaPacket.flags == 0)
                            fprintf(stdout, "%s\n", ipaPacket.buf);
                        else
                            fprintf(stderr, "%s\n", ipaPacket.buf);
                    }
                }
                else 
                {
                    break;
                }
            }
        }
        close(socketFD);
    }
    return 0;
}
