#ifndef IPA_PACKET_H
#define IPA_PACKET_H

typedef enum IPApacketType
{
    IPApacketType_none,
    IPApacketType_q,
    IPApacketType_status,
    IPApacketType_rm,
    IPApacketType_submit,
    IPApacketType_test,
    IPApacketType_windows,
    IPApacketType_windows_result,
    IPApacketType_close,
} IPApacketType;

typedef struct IPApacket
{
    int           magicNumber;
    IPApacketType packetType;
    int           flags;
    int           subflags;
    char          buf[1000];
} IPApacket;

#define IPA_PACKET_MAGIC_NUMBER 0x9557459

#endif
